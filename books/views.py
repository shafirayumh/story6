from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.
def books(request):
    return render(request, 'story8/books.html')