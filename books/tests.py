from django.test import LiveServerTestCase, TestCase, Client
from django.http import HttpRequest
from .views import *
from django.urls import resolve
from django.utils import timezone

# Create your tests here.
class Story8_Web_Test(TestCase):

    def test_books_response(self):
        self.response = Client().get('/books/')
        self.assertEqual(self.response.status_code, 200)

    def test_page_is_not_exist(self):
        self.response = Client().get('wek/')
        self.assertEqual(self.response.status_code, 404)

    def test_using_landing_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'story8/books.html') 

    def test_using_home_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books) 

    def test_tulisan_cari_buku(self):
        request = HttpRequest()
        response = books(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Mau Cari Buku Apa?', html_response)
    
    def test_form_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</form>', self.response.content.decode())