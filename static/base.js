$(document).ready(function () {
    var colorCondition = 0;
    $(".change").click(function(){
        $(".bg-light").toggleClass('bg-info');
        $(".bg-danger").toggleClass('bg-dark');
        $(".bg-secondary").toggleClass('bg-danger');
        // $(".navbar-light").toggleClass('navbar-dark')
        if (colorCondition === 0) {
            $(".navbar, .navbar-light .navbar-nav .nav-link").css(
                {"background-color": "black", "color": "white"}
            );
            $(".pink").toggleClass('dark');
            $(".pink").toggleClass('pink');
            $(".name").css({"color" : "white"});

        } else {
            $(".navbar, .navbar-light .navbar-nav .nav-link").css(
                {"background-color": "gray", "color": "white"}
            );
            $(".dark").toggleClass('pink');
            $(".dark").toggleClass('dark');
            $(".name").css({"color" : "black"});
        }
        colorCondition = (colorCondition + 1) % 2;
    });
});