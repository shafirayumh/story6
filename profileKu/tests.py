from django.test import LiveServerTestCase, TestCase, Client
from django.http import HttpRequest
from .views import *
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class Story7_Web_Test(TestCase):

    def test_landing_page_response(self):
        self.response = Client().get('/profile/')
        self.assertEqual(self.response.status_code, 200)

    def test_page_is_not_exist(self):
        self.response = Client().get('/wek/')
        self.assertEqual(self.response.status_code, 404)

    def test_using_landing_template(self):
        response = Client().get('/profile/')
        print(response.content.decode())
        self.assertTemplateUsed(response, 'story7/profile.html') 

    def test_using_home_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile) 

    def test_halo_apa_kabar(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Shafira Ayu Maharani', html_response)


class Story7_FunctionalTest(TestCase):
    def setUp(self):
            chrome_options = Options()
            chrome_options.add_argument('--dns-prefetch-disable')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
            super(Story7_FunctionalTest,self).setUp()

    def tearDown(self):
            self.selenium.quit()
            # super(Story7_FunctionalTest, self).tearDown()

    # def test_change_theme_background(self):
    #         self.selenium.get(self.live_server_url)
    #         css = self.selenium.find_element_by_tag_name('body')
    #         background = css.value_of_css_property('background-color')
    #         self.assertEqual(background, 'rgba(249, 231, 159, 1)')
    #         self.tearDown()
