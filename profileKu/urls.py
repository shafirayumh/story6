from django.urls import path
from . import views


app_name = 'profileKu'

urlpatterns = [
    path('', views.profile, name='profile'),  
]