from django.test import LiveServerTestCase, TestCase, Client
from django.http import HttpRequest
from .views import *
from django.urls import resolve
from .models import Status
from .forms import StatusForm
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class Story6_Web_Test(TestCase):

    def test_landing_page_response(self):
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)

    def test_page_is_not_exist(self):
        self.response = Client().get('wek/')
        self.assertEqual(self.response.status_code, 404)

    def test_using_landing_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story6/landing.html') 

    def test_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home) 

    def test_halo_apa_kabar(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, Apa Kabar?', html_response)
    
    def test_form_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</form>', self.response.content.decode())

class Story6_Model_Test(TestCase):
    def test_model_create_new_status(self):
        new_status = Status.objects.create(status='Hai uwu')

        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)
      
    # def test_save_POST_request(self):
    #     response = self.client.post('/add_status/', follow=True, data = {'status' : 'Uwu'})
    #     counting_status_object = Status.objects.all().count()
    #     self.assertEqual(counting_status_object, 1)

    #     self.assertEqual(response.status_code, 302)

    #     new_response = self.client.get('/landingpage/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('Uwu', html_response)

class Story6_Form_Test(TestCase):
    def test_forms_valid(self):
        form_data = {'status': 'aku cantik'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

class Story6_FunctionalTest(LiveServerTestCase):
	def setUp(self):
      
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Story6_FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		# super(Story6_FunctionalTest, self).tearDown()

	def test_landing_page_element_greet(self):
		self.selenium.get(self.live_server_url)
		time.sleep(3)
        
		greet = self.selenium.find_element_by_id('sayhello')
		self.assertEqual(greet.find_element_by_tag_name('h1').text, 'Halo, Apa Kabar?')

	def test_landing_page_status_form(self):
		self.selenium.get(self.live_server_url)
		time.sleep(3)

		status = self. selenium.find_element_by_name('status')
		submit = self.selenium.find_element_by_name('submit')

		message = 'Coba-Coba'
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertIn(message, self.selenium.page_source)

	def test_landing_page_status_form_more_than_300_char(self):
		self.selenium.get(self.live_server_url)
		time.sleep(3)

		status = self.selenium.find_element_by_name('status')
		submit = self.selenium.find_element_by_name('submit')

		message = 'Coba-Coba'*500
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertNotIn(message, self.selenium.page_source)



