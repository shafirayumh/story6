from django import forms
from .models import Status
from django.forms import ModelForm

class StatusForm(forms.Form):
    status = forms.CharField(max_length=300)
