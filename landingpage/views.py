from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

# Create your views here.
def home(request):
    list_status = Status.objects.all().order_by('-datetime')
    return render(request, "story6/landing.html", {'statusq' : list_status})

def add_status(request):
    if request.method == 'POST' :
        form = StatusForm(request.POST)
        if form.is_valid():
            stat = form.data['status']
            stats = Status(status=stat)
            stats.save()
    return redirect('/')