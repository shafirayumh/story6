from django.test import TestCase, Client
from django.urls import resolve, reverse

# Create your tests here.
class Story9TestLoginUser(TestCase):

    def test_oy_url_is_not_exist(self):
        client = Client()
        response = client.get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_login_template(self):
        client = Client()
        response = client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_logout_template(self):
        client = Client()
        response = client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)



# class Story9_Test_Buat_User(TestCase):
#     def test_create_user_exists(self):
#         response = self.client.get('/user/create/')

#         self.assertContains(response, 'Daftar')
#         self.assertContains(response, 'Username')
#         self.assertContains(response, 'Password')
#         self.assertContains(response, 'Password Konfirmasi')

#     def test_create_user(self):
#         response = self.client.post('/user/create/', follow=True, data={
#             'name': 'Fira',
#             'username': 'aiu',
#             'password1': '12345',
#             'password2': '12345',
#         })

#         self.assertRedirects(response, '/')
#         self.assertEqual(User.objects.count(), 1)
#         self.assertContains(response, 'Halo, Fira!')

#     def test_create_user_username_already_exist(self):
#         user = User.objects.create_user('aiu', '', '12345')
#         user.first_name = 'Fira'
#         user.save()

#         response = self.client.post('/user/create/', follow=True, data={
#             'name': 'Ayu',
#             'username': 'aiu',
#             'password1': '54321',
#             'password2': '5431',
#         })

#         self.assertEqual(User.objects.count(), 1)
#         self.assertContains(response, 'Maaf, Username sudah digunakan. Mohon mengganti username Anda')

#     def test_create_user_confirm_password_not_equal(self):
#         response = self.client.post('/user/create/', follow=True, data={
#             'name': 'Ayu',
#             'username': 'aiu',
#             'password1': '54321',
#             'password2': '5421',
#         })

#         self.assertContains(response, 'Konfirmasi password tidak sama.')


# class Story9TestLoginUser(TestCase):
#     def setUp(self):
#         self.user = User.objects.create_user('aiu', '', '12345')
#         self.user.first_name = 'Fira'
#         self.user.save()

#     def test_login_user_exist(self):
#         response = self.client.get('/user/login/')

#         self.assertContains(response, 'Masuk')
#         self.assertContains(response, 'Username')
#         self.assertContains(response, 'Password')

#     def test_login_user(self):
#         response = self.client.post('/user/login/', follow=True, data={
#             'username': 'aiu',
#             'password': '12345',
#         })

#         self.assertRedirects(response, '/')
#         self.assertContains(response, 'Halo, Fira!')

#     def test_login_user_failed(self):
#         response = self.client.post('/user/login/', follow=True, data={
#             'username': 'aiu',
#             'password': '123',
#         })

#         self.assertRedirects(response, '/user/login/', status_code=200)
#         self.assertContains(response, 'Username atau Password salah.')

#     def test_logout_user(self):
#         self.client.login(username='aiu', password='12345')
#         response = self.client.get('/user/logout/')

#         self.assertRedirects(response, '/')
#         self.assertContains(response, 'Anda berhasil keluar')